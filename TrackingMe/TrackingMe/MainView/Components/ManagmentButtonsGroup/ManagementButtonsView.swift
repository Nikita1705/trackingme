//
//  ManagementButtonsView.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/12/21.
//

import UIKit

protocol ManagementButtonsViewDelegate: class {
    func setManagementState(_ state: ManagementState)
}

public enum ManagementState {
    case start
    case stop
    case pause
}

final class ManagementButtonsView: UIView {
    
    var isModSelected: Bool = false {
        didSet {
            self.checkModeState(isSelected: self.isModSelected)
            self.checkButtonState()
        }
    }
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var stopBtn: UIButton!
    @IBOutlet private weak var pauseBtn: UIButton!
    @IBOutlet private weak var startBtn: UIButton!
    
    weak var delegate: ManagementButtonsViewDelegate?
    private var currentState: ManagementState = .stop
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        Bundle.main.loadNibNamed("ManagementButtonsView", owner: self, options: nil)
        self.contentView.fixInView(self)
        self.viewSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        Bundle.main.loadNibNamed("ManagementButtonsView", owner: self, options: nil)
        self.contentView.fixInView(self)
        self.viewSetup()
    }
    
    @IBAction func startAction() {
        self.currentState = .start
        self.isBtnHidden(false)
        self.delegate?.setManagementState(.start)
    }
    
    @IBAction func stopAction() {
        self.currentState = .stop
        self.isBtnHidden(true)
        self.delegate?.setManagementState(.stop)
    }
    
    @IBAction func pauseAction() {
        self.currentState = .pause
        self.isBtnHidden(true)
        self.delegate?.setManagementState(.pause)
    }
}

private extension ManagementButtonsView {
    func viewSetup() {
        self.pauseBtn.isHidden = true
        self.stopBtn.isEnabled = false
        self.startBtn.isEnabled = false
        self.checkButtonState()
    }
    
    func isBtnHidden(_ isHidden: Bool) {
        self.pauseBtn.isHidden = isHidden
        self.startBtn.isHidden = !isHidden
    }
    
    func checkModeState(isSelected: Bool) {
        self.stopBtn.isEnabled = isSelected
        self.startBtn.isEnabled = isSelected
    }
    
    func checkButtonState() {
        if self.startBtn.isEnabled == true {
            self.startBtn.backgroundColor = .green
            self.stopBtn.backgroundColor = .red
        } else {
            self.startBtn.backgroundColor = .gray
            self.stopBtn.backgroundColor = .gray
        }
    }
}
