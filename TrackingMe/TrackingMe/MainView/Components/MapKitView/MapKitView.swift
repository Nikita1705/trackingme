//
//  MapKitView.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/7/21.
//

import UIKit
import MapKit

protocol MapViewDelegate: class {
    func getNewAnnotation(modeType: ModeState)
    func getNewManageState(_ state: ManagementState)
}

final class MapKitView: UIView {

    @IBOutlet private weak var mapView: MKMapView!
    
    private var locations = [CLLocationCoordinate2D]()
    private var annotations = [ModeState : CustomCoordinate]()
    private var didStartDrawing: Bool = true
    private var managementState: ManagementState = .start
    private var modeState: ModeState? {
        didSet {
            self.addNewAnnotation()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        Bundle.main.loadNibNamed("MapKitView", owner: self, options: nil)
        self.mapView.fixInView(self)
        self.mapViewSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        Bundle.main.loadNibNamed("MapKitView", owner: self, options: nil)
        self.mapView.fixInView(self)
        self.mapViewSetup()
    }
}

extension MapKitView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if self.didStartDrawing, self.modeState != .plain, self.managementState == .start || self.managementState == .pause {
            self.locations.append(userLocation.coordinate)
            self.startDrawingRoute()
        } else {
            self.locations.removeAll()
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.strokeColor = .red
            pr.lineWidth = 5
            return pr
        }
        return MKOverlayRenderer()
    }
}

extension MapKitView: MapViewDelegate {
    func getNewManageState(_ state: ManagementState) {
        self.managementState = state
        switch self.managementState {
        case .start:
            self.didStartDrawing = true
        case .stop:
            self.didStartDrawing = false
        case .pause:
            self.didStartDrawing = false
        }
    }
    
    func getNewAnnotation(modeType: ModeState) {
        self.didStartDrawing = true
        self.modeState = modeType
    }
}

private extension MapKitView {
    func mapViewSetup() {
        self.mapView.frame = self.frame
        self.mapView.showsUserLocation = true
        self.mapView.userTrackingMode = .follow
        self.mapView.delegate = self
    }
    
    func addNewAnnotation() {
        guard let newState = self.modeState else { return }
        self.annotations[newState] = CustomCoordinate(lat: self.mapView.userLocation.coordinate.latitude, long: self.mapView.userLocation.coordinate.longitude)
        
        for (key, value) in self.annotations {
            self.mapView.addAnnotation(CustomPin(pinTitle: key.rawValue, location: CLLocationCoordinate2D(latitude: value.lat, longitude: value.long)))
        }
    }
    
    func startDrawingRoute() {
        let polyLine = MKPolyline(coordinates: self.locations, count: self.locations.count)
        self.mapView.addOverlay(polyLine)
    }
}
