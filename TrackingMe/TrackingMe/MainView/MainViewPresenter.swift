//
//  MainViewPresenter.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/12/21.
//

import Foundation
import CoreLocation

protocol MainViewPresenterProtocol: class {
    var mapViewDelegate: MapViewDelegate? { get set }
    var tripInfoViewDelegate: TripInfoViewDelegate? { get set }
    func numberOfRows() -> Int
    func elementForRow(at indexPath: IndexPath) -> Trip
}

final class MainViewPresenter: MainViewPresenterProtocol {
    
    var mapViewDelegate: MapViewDelegate?
    var tripInfoViewDelegate: TripInfoViewDelegate?
    
    private weak var view: MainViewProtocol!
    
    private var locationManager: LocationManagerProtocol!
    private var coreDataManager: CoreDataManagerProtocol?
    
    private var timer = Timer()
    private var stopedTime: Double = 0.0
    private var elapsedTimeText: String = ""
    
    private var modeType: ModeState? {
        didSet {
            self.didModeTypeChange()
            self.view.isModeSelected(true)
        }
    }
    private var managementState: ManagementState = .start {
        didSet {
            self.didManagementStateChange()
            
        }
    }
    private var data = Array<Trip>()
    
    init(view: MainViewProtocol, locationManager: LocationManagerProtocol, coreDataManager: CoreDataManagerProtocol) {
        self.view = view
        self.coreDataManager = coreDataManager
        self.locationManager = locationManager
        self.locationManager.enableLocation()
    }
    
    func numberOfRows() -> Int {
        self.data.count
    }
    
    func elementForRow(at indexPath: IndexPath) -> Trip {
        self.data[indexPath.row]
    }
}

extension MainViewPresenter: ManagementButtonsViewDelegate {
    func setManagementState(_ state: ManagementState) {
        self.managementState = state
    }
}

extension MainViewPresenter: ModesButtonsViewDelegate {
    func setCurrentModeState(_ state: ModeState) {
        self.modeType = state
    }
}

private extension MainViewPresenter {
    func didModeTypeChange() {
        guard let type = self.modeType else { return }
        self.mapViewDelegate?.getNewAnnotation(modeType: type)
    }
    
    func didManagementStateChange() {
        switch self.managementState {
        case .start:
            self.startTimer()
        case .stop:
            self.timer.invalidate()
            self.timerTicked()
            self.createNewTrip()
            self.stopedTime = 0.0
        case .pause:
            self.timer.invalidate()
            self.timerTicked()
            self.createNewTrip()
        }
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (timer) in
            self?.stopedTime += 0.1
        })
    }
    
    func timerTicked() {
        let hours = Int(self.stopedTime/3600)
        let minutes = Int(self.stopedTime / 60)
        let seconds = Int(self.stopedTime.truncatingRemainder(dividingBy: 60))
        let tenthsOfSecond = Int((self.stopedTime * 10).truncatingRemainder(dividingBy: 10))
        self.elapsedTimeText = String(format: "%02d:%02d:%02d.%d", hours, minutes, seconds, tenthsOfSecond)
    }
    
    func createNewTrip() {
        guard let modeType = self.modeType else { return }
        let distance = [TripInfo.distance : self.setCurrentDistance()]
        let duration = [TripInfo.duration : self.elapsedTimeText]
        let mode = [TripInfo.modeType : modeType.rawValue]
//        self.coreDataManager?.addNewTrip(info: [distance, duration, mode])
        self.tripInfoViewDelegate?.updateData()
    }
    
    func setCurrentDistance() -> String {
        String(format: "%.02f", self.locationManager.getDistance())
    }
}
