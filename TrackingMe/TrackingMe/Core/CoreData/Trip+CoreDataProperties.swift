//
//  Trip+CoreDataProperties.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/15/21.
//
//

import Foundation
import CoreData


extension Trip {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Trip> {
        return NSFetchRequest<Trip>(entityName: "Trip")
    }

    @NSManaged public var distance: String?
    @NSManaged public var duration: String?
    @NSManaged public var modeType: String?

}

extension Trip : Identifiable {

}
