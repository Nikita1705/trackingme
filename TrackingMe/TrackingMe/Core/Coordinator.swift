//
//  Coordinator.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/14/21.
//

import UIKit

protocol CoordinatorProtocol {
    func start()
}

final class Coordinator: CoordinatorProtocol {
    
    private var window: UIWindow?
    private var builder: BuilderProtocol
    private var coreDataManager: CoreDataManagerProtocol
    
    init(window: UIWindow?, coreDataManager: CoreDataManagerProtocol, builder: BuilderProtocol) {
        self.window = window
        self.coreDataManager = coreDataManager
        self.builder = builder
        self.windowSetup()
    }
    
    func start() {
        self.showMainView()
    }
    
    func showMainView() {
        let mainView = self.builder.createMain(coreDataManager: self.coreDataManager)
        self.window?.rootViewController = mainView as? MainView
    }
}

private extension Coordinator {
    func windowSetup() {
        self.window?.makeKeyAndVisible()
        self.window?.overrideUserInterfaceStyle = .light
        self.window?.backgroundColor = .white
    }
}
