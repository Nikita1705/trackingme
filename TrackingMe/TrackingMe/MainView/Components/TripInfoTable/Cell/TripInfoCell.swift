//
//  TripInfoCell.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/13/21.
//

import UIKit

typealias ValuePair = [[TripInfo : String]]

public enum TripInfo {
    case modeType
    case distance
    case duration
}

final class TripInfoCell: UITableViewCell {
    
    static let identifier = "TripInfoCell"
    static let height = UITableView.automaticDimension
    
    @IBOutlet private weak var modeTypeLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    @IBOutlet private weak var timeRangeLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.modeTypeLabel.text = nil
        self.distanceLabel.text = nil
        self.timeRangeLabel.text = nil
    }
    
    func updateWith(data: Trip) {
        self.modeTypeLabel.text = data.modeType
        self.distanceLabel.text = data.distance
        self.timeRangeLabel.text = data.duration
    }
}
