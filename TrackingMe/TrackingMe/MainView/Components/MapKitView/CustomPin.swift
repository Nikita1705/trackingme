//
//  CustomPin.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/14/21.
//

import CoreLocation
import MapKit

final class CustomPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    
    init(pinTitle:String, location:CLLocationCoordinate2D) {
        self.title = pinTitle
        self.coordinate = location
    }
}

struct CustomCoordinate: Hashable {
    var lat: Double
    var long: Double
}
