//
//  MainView.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/7/21.
//

import UIKit

protocol MainViewProtocol: class {
    func isModeSelected(_ isSelected: Bool)
}

final class MainView: UIViewController {
    
    static func initialize(locationManager: LocationManagerProtocol, coreDataManager: CoreDataManagerProtocol) -> UIViewController {
        let storyboard = UIStoryboard.init(name: "MainView", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier:"MainView") as! MainView
        controller.presenter = MainViewPresenter(view: controller, locationManager: locationManager, coreDataManager: coreDataManager)
        
        return controller
    }

    @IBOutlet private weak var mapKitView: MapKitView!
    @IBOutlet private weak var modesButtonsView: ModesButtonsView!
    @IBOutlet private weak var tripInfoView: TripInfoView!
    @IBOutlet private weak var managementButtonsView: ManagementButtonsView!
    
    private var presenter: MainViewPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
}

extension MainView: MainViewProtocol {
    func isModeSelected(_ isSelected: Bool) {
        self.managementButtonsView.isModSelected = isSelected
    }
}

private extension MainView {
    func setup() {
        self.presenter?.mapViewDelegate = self.mapKitView
        self.presenter?.tripInfoViewDelegate = self.tripInfoView
        self.managementButtonsView.delegate = self.presenter as? ManagementButtonsViewDelegate
        self.modesButtonsView.delegate = self.presenter as? ModesButtonsViewDelegate
    }
}
