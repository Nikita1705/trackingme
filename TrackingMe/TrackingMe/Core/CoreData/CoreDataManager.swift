//
//  CoreDataManager.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/14/21.
//

import Foundation
import CoreData

protocol CoreDataManagerProtocol {
    func addNewTrip(info: ValuePair)
}

final class CoreDataManager {
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TripInfo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension CoreDataManager: CoreDataManagerProtocol {
    func addNewTrip(info: ValuePair) {
        
        let trip = Trip(context: self.persistentContainer.viewContext)
        info.forEach({
            trip.distance = $0[.distance]
            trip.duration = $0[.duration]
            trip.modeType = $0[.modeType]
        })
        do {
            try self.persistentContainer.viewContext.save()
            print("Saving is successfully")
        } catch let error as NSError {
            print("Trip data saving is failure: \(error.userInfo)")
        }
    }
}
