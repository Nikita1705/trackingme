//
//  LocationManager.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/12/21.
//

import Foundation
import CoreLocation

protocol LocationManagerProtocol {
    func enableLocation()
    func getDistance() -> Double
}

final class LocationManager: NSObject, LocationManagerProtocol {
    
    private var locationManage = CLLocationManager()
    private var locations = [CLLocation]()
    private var currentDistance: Double = 0.0
    
    override init() {
        super.init()
        self.locationSetup()
    }
    
    func enableLocation() {
        switch self.locationManage.authorizationStatus {
        case .notDetermined:
            self.locationManage.requestWhenInUseAuthorization()
        case .restricted, .denied:
            break
        case .authorizedAlways:
            self.locationManage.startUpdatingLocation()
        case .authorizedWhenInUse:
            self.locationManage.requestAlwaysAuthorization()
        @unknown default:
            break
        }
    }
    
    func getDistance() ->  Double {
        self.currentDistance / 1000
    }
}

private extension LocationManager {
    func locationSetup() {
        self.locationManage.delegate = self
        self.locationManage.startUpdatingLocation()
        self.locationManage.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManage.distanceFilter = 5
    }
    
    func calculateDistance(locations: [CLLocation]) {
        guard let startPointLocation = locations.first,
              let finishPointLocation = locations.last
        else {
            return
        }
        self.currentDistance = startPointLocation.distance(from: finishPointLocation)
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.locationManage.requestAlwaysAuthorization()
        } else if status == .authorizedAlways {
            self.locationManage.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locations.append(contentsOf: locations)
        self.calculateDistance(locations: self.locations)
    }
}
