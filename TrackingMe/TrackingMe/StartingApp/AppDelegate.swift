//
//  AppDelegate.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/7/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: CoordinatorProtocol?
    lazy var coreDataManager = CoreDataManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let builder = Builder()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.coordinator = Coordinator(window: self.window, coreDataManager: self.coreDataManager, builder: builder)
        self.coordinator?.start()
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.coreDataManager.saveContext()
    }
}

