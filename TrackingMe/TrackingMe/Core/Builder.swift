//
//  Builder.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/12/21.
//

import Foundation

protocol BuilderProtocol {
    func createMain(coreDataManager: CoreDataManagerProtocol) -> MainViewProtocol
}

final class Builder: BuilderProtocol {
    
    func createMain(coreDataManager: CoreDataManagerProtocol) -> MainViewProtocol {
        let locationManager = LocationManager()
        let mainView = MainView.initialize(locationManager: locationManager, coreDataManager: coreDataManager)
        
        return mainView as! MainViewProtocol
    }
}
