//
//  ModesButtonsView.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/12/21.
//

import UIKit

protocol ModesButtonsViewDelegate: class {
    func setCurrentModeState(_ state: ModeState)
}

public enum ModeState: String {
    case bicycle = "Bicycle"
    case foot = "Foot"
    case plain = "Plain"
}

final class ModesButtonsView: UIView {
    
    @IBOutlet private weak var contentView: UIView!
    
    weak var delegate: ModesButtonsViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        Bundle.main.loadNibNamed("ModesButtonsView", owner: self, options: nil)
        self.contentView.fixInView(self)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        Bundle.main.loadNibNamed("ModesButtonsView", owner: self, options: nil)
        self.contentView.fixInView(self)
    }
    
    @IBAction func bicycleMode() {
        self.delegate?.setCurrentModeState(.bicycle)
    }
    
    @IBAction func footMode() {
        self.delegate?.setCurrentModeState(.foot)
    }
    
    @IBAction func plainMode() {
        self.delegate?.setCurrentModeState(.plain)
    }
}
