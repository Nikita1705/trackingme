//
//  TripInfoView.swift
//  TrackingMe
//
//  Created by Nikita Moskalenko on 5/13/21.
//

import UIKit

protocol TripInfoViewDelegate: class {
    func updateData()
}

final class TripInfoView: UIView {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var rowsCount: Int = 0
    private var elementForRow: Trip?
    
    init(rowsCount: Int, elementForRow: Trip) {
        self.elementForRow = elementForRow
        self.rowsCount = rowsCount
        super.init(frame: .zero)
        Bundle.main.loadNibNamed("TripInfoView", owner: self, options: nil)
        self.setup()
        self.tableView.fixInView(self)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        Bundle.main.loadNibNamed("TripInfoView", owner: self, options: nil)
        self.setup()
        self.tableView.fixInView(self)
    }
}

extension TripInfoView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        TripInfoCell.height
    }
}

extension TripInfoView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.rowsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TripInfoCell.identifier, for: indexPath) as? TripInfoCell else { fatalError("TripInfoCell isn't connect")}
//        cell.updateWith(data: Trip()
        return cell
    }
}

extension TripInfoView: TripInfoViewDelegate {
    func updateData() {
        self.tableView.reloadData()
    }
}

private extension TripInfoView {
    func setup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: TripInfoCell.identifier, bundle: nil), forCellReuseIdentifier: TripInfoCell.identifier)
        self.tableView.tableFooterView = UIView()
    }
}
